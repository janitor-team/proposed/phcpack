#!/usr/bin/env python3

import phcpy

# phcpy.examples.test()
# this doesn't completely work, so we include the parts that do:
phcpy.phcpy2c3.py2c_set_seed(834798279)
phcpy.examples.solve_binomials()
phcpy.examples.solve_cyclic7()
phcpy.examples.solve_sysd1()
phcpy.examples.solve_fbrfive4()
phcpy.examples.solve_game4two()
phcpy.examples.solve_katsura6()
phcpy.examples.solve_noon3()
phcpy.examples.solve_rps10()
phcpy.examples.solve_stewgou40()
phcpy.examples.solve_tangents()
# https://github.com/janverschelde/PHCpack/issues/44
# phcpy.examples.solve_sevenbar()

phcpy.solver.test()
phcpy.solutions.test_class()
